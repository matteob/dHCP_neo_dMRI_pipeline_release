#!/bin/bash

# Master script for launching dHCP diffusion pipeline
#
# Matteo Bastiani - University of Nottingham & University of Oxford
# Saad Jbabdi - University of Oxford
#
# Copyright (C) 2019 University of Oxford 
export LC_ALL=C
# Stopping when error is encountered
set -e

Usage(){
    echo ""
    echo "usage: `basename $0` <data folder> <session folder> <data file> <connectome ID> <acquisition protocol> <slspec> <output folder> <age at scan> <age at birth> <subject T2> <subject segmentation> <superres flag> <gpu flag>"
    echo "Required arguments"
    echo ""
    echo "-d                  data folder"
    echo "-s                  session name"
    echo "-f                  data file name"
    echo "-o                  output folder"
    echo "--cid               unique subject identifier"
    echo "--protocol          acquisition protocol file"
    echo "--slspec            slice specification file"
    echo "Arguments for registration steps"
    echo ""
    echo " --T2folder         subject T2w folder"
    echo " --Regfolder        subject reg folder from FMRI pipeline"
    echo " --age_scan         PMA in weeks at scan (rounded)"
    echo " --age_birth        PMA in weeks at birth (rounded)"
    echo ""
    echo " Lazy arguments"
    echo ""
    echo " --nogpu            turn off GPU processing"
    echo " --nosuperres       turn off super-resolution"
    echo " --noslicetovol     turn off slice-to-volume in Eddy"
    echo " --nosuscbymot      turn off susceptibility-by-motion in Eddy"
    echo " --noreg            turn off registration step"
    echo " --stopat           0:import, 1:topup, 2:eddy, 3:superres, 4:post-eddy, 5:bpx, 6:reg, 7:all"
    echo " --rerun            rerun processing stages even if they have already been completed"
    echo ""
    echo " -h,--help          display help message"
    echo "The script will queue all the jobs necessary to process dHCP neonatal dMRI data"
}

log_args(){
    o=$1
    shift 1
    rm -rf $o
    for arg in $*;do
	echo -e "$arg\t : ${!arg}"
	echo -e "$arg\t : ${!arg}" >> $o
    done
}
print_args(){
    for arg in $*;do
	echo -e "$arg\t : ${!arg}"
    done
}

# get arg for - options (need to pass both $1 and $2 to this)
get_arg() {
    if [ X$2 = X ] ; then
	echo "Option $1 requires an argument" 1>&2
	exit 1
    fi
    echo $2
}
# get image filename 
get_imarg() {
    arg=`get_arg $1 $2`;
    arg=`$FSLDIR/bin/remove_ext $arg`;
    echo $arg
}

# Parse command-line options
# Default values
dataFolder=
sessionFolder=
dataFile=
outFolder=
connID=
acqProt=
slspec=
subjT2Folder=
subjRegfolder=
age_scan="N/A"
age_birth="N/A"
t2seg=
srFlag=1
gpuFlag=1
s2vFlag=1
sbmFlag=1
regFlag=1
eddy_opt=
stopat=8
rerun=0


if [ $# -eq 0 ] ; then Usage; exit 0; fi
while [ $# -ge 1 ];do
    iarg=$1;
    case "$iarg"
    in
	-d)
	    dataFolder=`get_arg $1 $2`
	    shift 2;;
	-s)
	    sessionFolder=`get_arg $1 $2`
	    shift 2;;
	-f)
	    dataFile=`get_arg $1 $2`
	    shift 2;;
	-o)
	    outFolder=`get_arg $1 $2`
	    shift 2;;
	--cid)
	    connID=`get_arg $1 $2`
	    shift 2;;
	--protocol)
	    acqProt=`get_arg $1 $2`
	    shift 2;;
	--slspec)
	    slspec=`get_arg $1 $2`
	    shift 2;;
	--T2folder)
	    subjT2Folder=`get_imarg $1 $2`
	    shift 2;;
	--age_scan)
	    ageScan=`get_arg $1 $2`
	    shift 2;;
	--age_birth)
	    ageBirth=`get_arg $1 $2`
	    shift 2;;
	--Regfolder)
	    subjRegfolder=`get_imarg $1 $2`
	    shift 2;;
	--nosuperres)
	    srflag=0
	    shift 2;;
	--nogpu)
	    gpuFlag=0
	    eddy_opt="$eddy_opt --nogpu"
	    shift;;
	--noslicetovol)
	    s2vFlag=0
	    eddy_opt="$eddy_opt --noslicetovol"
	    shift;;
	--nosuscbymot)
	    sbmFlag=0
	    eddy_opt="$eddy_opt --nosuscbymot"
	    shift;;
	--noreg)
	    regFlag=0
	    shift;;
	--stopat)
	    stopat=`get_arg $1 $2`
	    shift 2;;
	--rerun)
	    rerun=1 
	    shift ;;
	-h)
	    Usage;
	    exit 0;;
	--help)
	    Usage;
	    exit 0;;
	*)
	    echo "Unrecognised option $1" 1>&2
	    exit 1	    			       
    esac
	
done
arglist="dataFolder sessionFolder dataFile outFolder connID acqProt slspec age_scan age_birth subjT2Folder subjRegfolder srFlag gpuFlag s2vFlag sbmFlag regFlag stopat rerun"
print_args $arglist

### Sanity checking of arguments
if [ X$dataFolder == X ] || [ X$sessionFolder == X ] || [ X$dataFile == X ] || [ X$outFolder == X ] || [ X$connID == X ] || [ X$acqProt == X ] || [ X$slspec == X ] ;then
    echo "One of the compolsury arguments is not set. try the -h option to see the list of required arguments"
    exit 1
fi

# Choose queue
if [ "${gpuFlag}" -eq "1" ]; then
    queue="cuda.q"
else
    queue="long.q"
fi


# Hard-coded option
noB0s=2                # Number of B0 volumes for each PE direction used to estimate distortions with TOPUP

# Number of acquired volumes
dimt4=`${FSLDIR}/bin/fslval ${dataFolder}/${sessionFolder}/DWI/${dataFile} dim4`
# Do a sanity check on the data? e.g. #slices etc.?



#============================================================================
# Create directory structure and write ages
#============================================================================
subjOutFolder=${outFolder}/${connID}/${sessionFolder}

prepFolder=${subjOutFolder}/PreProcessed
anatFolder=${subjOutFolder}/T2w
diffFolder=${subjOutFolder}/Diffusion
rawFolder=${subjOutFolder}/raw

mkdir -p ${prepFolder}/topup
mkdir -p ${prepFolder}/eddy
mkdir -p ${prepFolder}/logs
mkdir -p ${anatFolder}
mkdir -p ${diffFolder}
mkdir -p ${rawFolder}

# log command arg
logit=" -l ${prepFolder}/logs"

# If re-run, deleted status
if [ $rerun -eq 1 ];then
    rm -f ${prepFolder}/.status
fi

# Try to find age at scan
if [ $ageScan == "N/A" ];then
    if [ -f ${dataFolder}/sub-${connID}_sessions.tsv ];then
	sess=`echo $sessionFolder | sed s@"ses-"@@g`
	ageScan=`cat ${dataFolder}/sub-${connID}_sessions.tsv | grep ${sess} | awk '{print $2}'`
	echo "Detected age scan = $ageScan"
    fi
fi

echo "${ageScan}"   > ${subjOutFolder}/age
echo "${ageBirth}"  > ${subjOutFolder}/birth


# Display chosen options (copy to LOG file)
LOGFILE=${prepFolder}/logs/setJobs_log
log_args $LOGFILE $arglist


check_complete(){
    stage=$1
    if [ ! -f $subjOutFolder/.status ];then 
	echo 0
    else
	status=`cat $subjOutFolder/.status | grep "Stage $stage" | awk '{print $2}'`
	if [ X$status == X$stage ];then echo 1;else echo 0;fi
    fi
}


#============================================================================
# Prepare super-resolution commands (if needed)
#============================================================================
if [ "${srFlag}" -eq "1" ]; then
    ${scriptsFolder}/superres/setup_superres.sh ${prepFolder}/eddy/eddy_corrected.nii.gz ${prepFolder}/eddy/nodif_brain_mask.nii.gz ${prepFolder}/eddy/sr_commands.txt ${dimt4}
fi

echo -e "\n START: Setting up jobs for the dHCP neonatal dMRI data processing pipeline"

#============================================================================
# Set main chain starting from last completed step
#============================================================================

    
#============================================================================
# 0 : Import files and create initial structure
#============================================================================
waitforme=""
if [ $rerun -eq 1 ] || [ `check_complete 0` -eq 0 ];then
    importid=`${FSLDIR}/bin/fsl_sub -q short.q ${logit} -N dHCP_0_import ${scriptsFolder}/dHCP_neo_dMRI_importFiles.sh ${dataFolder}/${sessionFolder}/DWI ${dataFile} ${subjOutFolder} ${acqProt} ${dimt4} ${noB0s}`
    waitforme="-j $importid"
else
    echo "Stage 0 completed - skipping"
fi
if [ $stopat -eq 0 ];then exit 0;fi

#============================================================================
# 1 : Run TOPUP
#============================================================================
if [ $rerun -eq 1 ] || [ `check_complete 1` -eq 0 ];then
    topupid=`${FSLDIR}/bin/fsl_sub -q long.q ${waitforme} ${logit} -N dHCP_1_topup ${scriptsFolder}/dHCP_neo_dMRI_runTopup.sh ${subjOutFolder}`
    waitforme="-j $topupid"
else
    echo "Stage 1 completed - skipping"
fi
if [ $stopat -eq 1 ];then exit 0;fi

#============================================================================
# 2 : Run EDDY
#============================================================================
if [ $rerun -eq 1 ] || [ `check_complete 2` -eq 0 ];then
    eddyid=`${FSLDIR}/bin/fsl_sub -q ${queue} ${waitforme} ${logit} -N dHCP_2_eddy ${scriptsFolder}/dHCP_neo_dMRI_runEddy.sh ${subjOutFolder} ${slspec} ${eddy_opt}`
    waitforme="-j $eddyid"
else
    echo "Stage 2 completed - skipping"
fi
if [ $stopat -eq 2 ];then exit 0;fi

#============================================================================
# 3 : Super resolution for EDDY corrected data (if needed)
#============================================================================
if [ "${srFlag}" -eq "1" ]; then
    if [ $rerun -eq 1 ] || [ `check_complete 3a` -eq 0 ];then	
        pre_srid=`${FSLDIR}/bin/fsl_sub -q veryshort.q ${waitforme} ${logit} -N dHCP_3a_sr ${scriptsFolder}/superres/prerun_superres.sh ${prepFolder}/eddy/eddy_corrected.nii.gz`
	waitforme="-j $pre_srid"
    else
	echo "Stage 3a completed - skipping"
    fi
    if [ $rerun -eq 1 ] || [ `check_complete 3bc` -eq 0 ];then
        srid=`${FSLDIR}/bin/fsl_sub -q short.q ${waitforme} ${logit} -N dHCP_3b_sr -t ${prepFolder}/eddy/sr_commands.txt`
	waitforme="-j $srid"
        post_srid=`${FSLDIR}/bin/fsl_sub -q veryshort.q ${waitforme} ${logit} -N dHCP_3c_sr ${scriptsFolder}/superres/postrun_superres.sh ${prepFolder}/eddy/eddy_corrected.nii.gz`
	waitforme="-j $post_srid"
    else
	echo "Stage 3b/3c completed - skipping"
    fi
fi



if [ $stopat -eq 3 ];then exit 0;fi


#============================================================================
# 4 : Run EDDY post processing steps and DT fit
#============================================================================
if [ $rerun -eq 1 ] || [ `check_complete 4` -eq 0 ];then
    pprocid=`${FSLDIR}/bin/fsl_sub -q veryshort.q ${waitforme} ${logit} -N dHCP_4_posteddy ${scriptsFolder}/dHCP_neo_dMRI_runPostProc.sh ${subjOutFolder} ${srFlag} 1`
    waitforme="-j $pprocid"
else
    echo "Stage 4 completed - skipping"
fi
if [ $stopat -eq 4 ];then exit 0;fi

#============================================================================
# 5 : Run BPX model
#============================================================================
if [ $rerun -eq 1 ] || [ `check_complete 4` -eq 0 ];then
    bpxid=`${FSLDIR}/bin/fsl_sub -q ${queue} $waitforme ${logit} -N dHCP_5_bpx ${scriptsFolder}/dHCP_neo_dMRI_runBPX.sh ${diffFolder}`
    waitforme="-j $bpxid"
else
    echo "Stage 5 completed - skipping"
fi


if [ $stopat -eq 5 ];then exit 0;fi

    
#============================================================================
# 6 : NO LONGER NEEDED - Extract tissue and brain masks from segmented volume
#============================================================================
if [ $regFlag -eq 1 ];then
    if [ $rerun -eq 1 ] || [ `check_complete 7` -eq 0 ];then
#
#	maskid=`${FSLDIR}/bin/fsl_sub -q veryshort.q $waitforme ${logit} -N dHCP_6_masks  ${scriptsFolder}/dHCP_neo_dMRI_getMasks.sh ${subjOut#Folder} ${subjSeg}`
#	waitforme="-j $maskid"
#============================================================================
# 7 : Register diffusion to structural T2 (native and template spaces)
#============================================================================
	regid=`${FSLDIR}/bin/fsl_sub -q long.q $waitforme ${logit} -N dHCP_7_reg ${scriptsFolder}/dHCP_neo_dMRI_runRegistration.sh ${subjOutFolder} ${subjT2Folder} ${subjRegfolder}`
	waitforme="-j $regid"
    else
	echo "Stage 7 completed - skipping"
    fi
fi

if [ $stopat -eq 7 ];then exit 0;fi
    

#============================================================================
# 8 : Generate PNGs for QC reports
#============================================================================
if [ $rerun -eq 1 ] || [ `check_complete 8` -eq 0 ];then
    pngid=`${FSLDIR}/bin/fsl_sub -q veryshort.q ${waitforme} ${logit} -N dHCP_8_QC ${scriptsFolder}/utils/generateFigs.sh ${subjOutFolder}`
else
    echo "Stage 8 completed - skipping"
fi


echo -e "\n END: Setting up jobs for the dHCP neonatal dMRI data processing pipeline"
