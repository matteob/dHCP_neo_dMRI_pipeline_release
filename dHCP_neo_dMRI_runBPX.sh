#!/bin/bash
set -e
echo -e "\n START: running BPX model 3..."

convertsecs() {
 ((h=${1}/3600))
 ((m=(${1}%3600)/60))
 ((s=${1}%60))
 printf "%02d:%02d:%02d\n" $h $m $s
}

start=`date +%s`


diffFolder=$1


#============================================================================
# Run BPX model 3
#============================================================================
${FSLDIR}/bin/bedpostx ${diffFolder} -n 3 -b 3000 -model 3 --Rmean=0.3


end=`date +%s`

runtime=$((end-start))
TIME=$(convertsecs $runtime)
echo "Total runtime = $TIME"

echo "Stage 5 complete (${TIME})" >> $subFolder/.status


echo -e "\n END: runBPX"

