#!/bin/bash
set -e
echo -e "\n START: postrun_superres"

convertsecs() {
 ((h=${1}/3600))
 ((m=(${1}%3600)/60))
 ((s=${1}%60))
 printf "%02d:%02d:%02d\n" $h $m $s
}
start=`date +%s`


if [ "$1" == "" ];then
    echo ""
    echo "usage: $0 <4Dfile> "
    echo "       Post SuperResolution script"
    echo ""
    echo "       4Dfile: raw 4D data file"
    echo ""
    exit 1
fi

dataFile=$1

pathToFile=`dirname ${dataFile}` 

# Merge the results and clean temporary files/folders
${FSLDIR}/bin/fslmerge -t ${pathToFile}/data_sr `cat ${pathToFile}/tmp/fileList`

rm -rf ${pathToFile}/tmp


end=`date +%s`

runtime=$((end-start))
TIME=$(convertsecs $runtime)
echo "Total runtime = $TIME"

#===============
# Status
#===============
echo "Stage 3bc complete (${TIME} for c)" >> ${pathToFile}/../../.status


echo -e "\n END: postrun_superres"
