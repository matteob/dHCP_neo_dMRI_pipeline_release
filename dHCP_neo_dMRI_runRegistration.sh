#!/bin/bash


set -e
echo -e "\n START: runRegistration"

convertsecs() {
 ((h=${1}/3600))
 ((m=(${1}%3600)/60))
 ((s=${1}%60))
 printf "%02d:%02d:%02d\n" $h $m $s
}

start=`date +%s`


unset POSIXLY_CORRECT 


if [ "$1" == "" ];then
    echo ""
    echo "Registration script"
    echo ""
    echo "usage: $0 <SubjFolder> <SubjT2Folder> <subjRegFolder>"
    echo "       "
    echo "       SubjFolder:     Path to the subject processing folder"
    echo "       SubjT2Folder:   Path to subject T2w folder"
    echo "       subjRegFolder:  Path to subject reg folder"
    echo ""
    exit 1
fi


subjOutFolder=$1         # Path to the subject folder
subjT2folder=$2          # Path to subject T2 folder
subjRegfolder=$3         # Path to reg folder produced by FMRRI pipeline

prepFolder=${subjOutFolder}/PreProcessed
anatFolder=${subjOutFolder}/T2w
diffFolder=${subjOutFolder}/Diffusion

mkdir -p ${diffFolder}/xfms
mkdir -p $anatFolder


#============================================================================
# Copy T2w volume and wm mask in processing folder
#============================================================================
echo "Copying structural files"
${FSLDIR}/bin/imcp ${subjT2folder}/T2w ${anatFolder}/T2w
${FSLDIR}/bin/imcp ${subjT2folder}/T2w_wmmask ${anatFolder}/T2w_wmmask
${FSLDIR}/bin/fslmaths ${subjT2folder}/T2w -mas ${subjT2folder}/T2w_brainmask ${anatFolder}/T2w_brain


#============================================================================
# Register dMRI data to structural T2w using mean_b1000 volume and BBR 
#============================================================================

echo "Running FLIRT diff->struct using BBR"
${FSLDIR}/bin/flirt -in ${diffFolder}/att_b1000.nii.gz \
    -ref ${anatFolder}/T2w_brain.nii.gz \
    -out ${diffFolder}/xfms/diff2str \
    -omat ${diffFolder}/xfms/diff2str.mat \
    -bins 256 -cost bbr -wmseg ${anatFolder}/T2w_wmmask.nii.gz \
    -searchrx -90 90 -searchry -90 90 -searchrz -90 90 -dof 6  -interp spline

${FSLDIR}/bin/convert_xfm -omat ${diffFolder}/xfms/str2diff.mat -inverse ${diffFolder}/xfms/diff2str.mat


#============================================================================
# Concatenate diff2str with str2standard
#============================================================================
echo "Forming warps"

warp=$subjRegfolder/struct_to_standard_warp
convertwarp -m  ${diffFolder}/xfms/diff2str.mat -r ${anatFolder}/T2w -w $warp -o ${diffFolder}/xfms/diff2standard_warp
invwarp -w ${diffFolder}/xfms/diff2standard_warp -r ${diffFolder}/dtifit_b1000/dti_FA -o ${diffFolder}/xfms/standard2diff_warp

#============================================================================                                             
# Produce QC plots to check the registration
#============================================================================   
echo "Saving QC plots"
                                          
$FSLDIR/bin/fslmaths ${anatFolder}/T2w_wmmask -dilF -sub ${anatFolder}/T2w_wmmask -bin ${anatFolder}/T2w_wmmask_edges
$FSLDIR/bin/overlay 0 0 ${diffFolder}/xfms/diff2str.nii.gz 0 0.7 ${anatFolder}/T2w_wmmask_edges 0.1 1 ${prepFolder}/QC/regOverlay
$FSLDIR/bin/slicer ${subjOutFolder}/PreProcessed/QC/regOverlay -L -S 9 1160 ${subjOutFolder}/PreProcessed/QC/regOverlay.png



end=`date +%s`

runtime=$((end-start))
TIME=$(convertsecs $runtime)
echo "Total runtime = $TIME"

echo "stage 7 complete (${TIME})" >> ${subjOutFolder}/.status

echo -e "\n END: runRegistration"
