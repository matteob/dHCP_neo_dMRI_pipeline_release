#!/usr/bin/env fslpython

import sys, os
import numpy as np
from fsl.wrappers import flirt, LOAD
from fsl.data.image import Image

def parse_args():
    nargs = len(sys.argv) - 1
    if nargs < 5:

        print(
        """
           The script will select the best N b0 volumes for each PE direction based on topup results"
    
           usage: pickBestB0s.py <data> <bvals> <index> <readout> <n> <output>"
    
           data: 4D raw data file to sort"
           bvals: bvals file"
           index: eddy index file"
           readout: total readout time (in seconds)"
           n: number of b0s to select"
           output: output folder"
     
        """
        )
        exit()


def get_indices(bvals,pes,tol=100):
    ret = [ [] for _ in range(int(max(pes))) ]
    for i,(b,pe) in enumerate(zip(bvals,pes)):
        if b < tol:
            ret[int(pe)-1].extend([i])
    return ret
    
def extract_data(data,inds):
    return [data[:,:,:,i] for i in inds]

def normalise(x):
    return (x-x.mean()) / np.linalg.norm(x-x.mean())

def calc_corr(x,y):
    return np.sum(normalise(x.flatten())*normalise(y.flatten()))

def pairwise_compare(datalist,with_flirt=False):
    n    = len(datalist)
    corr = np.zeros((n,n))
    for i,data_i in enumerate(datalist):
        for j,data_j in enumerate(datalist):
            if i != j:
                if with_flirt:
                    i2j = flirt(Image(data_i),Image(data_j),dof=6,out=LOAD,omat=LOAD)
                    i2j = i2j['out'].data 
                else:
                    i2j = data_i
                corr[i,j] = calc_corr(i2j,data_j)


    return corr
def get_best_corr(corr,n):
    scores = np.sum(corr,axis=1)
    ind,sc = np.argsort(-scores),-np.sort(-scores)
    return ind[:n],sc[:n]

    

def main():
    parse_args()
    datafile     = sys.argv[1]
    bvalsfile    = sys.argv[2]
    eddyIdxFile  = sys.argv[3]
    ro_time      = sys.argv[4]   # Ignored for now
    no_b0s       = int(sys.argv[5])
    outfolder    = sys.argv[6]


    data         = Image(datafile)
    
    bvals        = np.loadtxt(bvalsfile)
    pes          = np.loadtxt(eddyIdxFile)

    
    pe_indices   = get_indices(bvals,pes)


    best_b0s_inds = []
    best_b0s_scores = []
    for inds in pe_indices:
        datalist   = extract_data(data,inds)
        corr       = pairwise_compare(datalist,with_flirt=False)
        sel,scores = get_best_corr(corr,no_b0s)        
        best_b0s_inds.extend( [inds[i] for i in sel] )
        best_b0s_scores.extend(scores)


    print(f'Best b0s : {best_b0s_inds}')
    print(f'Scores   : {best_b0s_scores}')

    os.makedirs(outfolder,exist_ok=True)

    np.savetxt(os.path.join(outfolder,'selected_b0s.txt'),best_b0s_inds,fmt='%d')
    np.savetxt(os.path.join(outfolder,'scores_b0s.txt'),best_b0s_scores)

    best_b0_ind = best_b0s_inds[np.argsort(best_b0s_scores)[-1]]
    print(f'best b0 = {best_b0_ind}')
    np.savetxt(os.path.join(outfolder,'ref_b0.txt'),[best_b0_ind])

    
    best_b0s = [data[:,:,:,i] for i in best_b0s_inds]
    best_b0s = np.transpose(np.asarray(best_b0s),[1,2,3,0])
    best_b0s = Image(best_b0s,header=data.header)
    best_b0s.save(os.path.join(outfolder,'phase'))
    
if __name__ == '__main__':
    main()
