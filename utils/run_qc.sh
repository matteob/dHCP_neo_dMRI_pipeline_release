#!/bin/bash
set -e


getopt(){
    cat $1 | sed s/"="/" "/g | grep -oP "(?<=--${2} )[^ ]+"
}

quad_command(){
    basename=$1  # eddy basename                                                                                       
    f=${basename}.eddy_command_txt
    eddyBase=$basename
    bvals=`getopt $f bvals`
    mask=`getopt $f mask`
    eddyParams=`getopt $f acqp`
    eddyIdx=`getopt $f index`
    bvecs=`getopt $f bvecs`
    slspec=`getopt $f slspec`
    field=$2
    echo "eddy_quad $eddyBase --bvals $bvals --mask $mask --field $field --eddyParams $eddyParams --eddyIdx $eddyIdx --bvecs $bvecs --slspec $slspec "
}

convertsecs() {
 ((h=${1}/3600))
 ((m=(${1}%3600)/60))
 ((s=${1}%60))
 printf "%02d:%02d:%02d\n" $h $m $s
}
start=`date +%s`

sFolder=$1


# EDDY QUAD
echo "Run QUAD"
rm -rf ${sFolder}/PreProcessed/eddy/eddy_corrected.qc
field=${sFolder}/PreProcessed/topup/fieldmap
eval `quad_command ${sFolder}/PreProcessed/eddy/eddy_corrected $field`

end=`date +%s`

runtime=$((end-start))
TIME=$(convertsecs $runtime)
echo "Total runtime = $TIME"


