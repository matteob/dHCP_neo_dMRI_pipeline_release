#!/bin/bash
set -e
echo -e "\n START: generatePNGs"



getopt(){
    cat $1 | sed s/"="/" "/g | grep -oP "(?<=--${2} )[^ ]+"
}

quad_command(){
    basename=$1  # eddy basename                                                                                       
    f=${basename}.eddy_command_txt
    eddyBase=$basename
    bvals=`getopt $f bvals`
    mask=`getopt $f mask`
    eddyParams=`getopt $f acqp`
    eddyIdx=`getopt $f index`
    bvecs=`getopt $f bvecs`
    slspec=`getopt $f slspec`
    field=$2
    echo "eddy_quad $eddyBase --bvals $bvals --mask $mask --field $field --eddyParams $eddyParams --eddyIdx $eddyIdx --bvecs $bvecs -\
-slspec $slspec "
}

convertsecs() {
 ((h=${1}/3600))
 ((m=(${1}%3600)/60))
 ((s=${1}%60))
 printf "%02d:%02d:%02d\n" $h $m $s
}
start=`date +%s`

sFolder=$1

app_list=" "
hor_gap=5
ver_gap=1

echo "Generate PNGs"
if [ `$FSLDIR/bin/imtest ${sFolder}/Diffusion/xfms/diff2str.nii.gz` -eq 1 ];then
    $FSLDIR/bin/overlay 0 0 ${sFolder}/Diffusion/xfms/diff2str.nii.gz 0 0.7 ${sFolder}/T2w/segmentation/wm_mask_edges.nii.gz 0.1 1 ${sFolder}/PreProcessed/QC/regOverlay
    $FSLDIR/bin/slicer ${sFolder}/PreProcessed/QC/regOverlay -L -S 9 1160 ${sFolder}/PreProcessed/QC/regOverlay.png
fi

$FSLDIR/bin/slicer ${sFolder}/Diffusion/mean_b0.nii.gz -i 0 60 -a ${sFolder}/PreProcessed/QC/b0.png
$FSLDIR/bin/slicer ${sFolder}/Diffusion/mean_b400.nii.gz -i 0 40 -a ${sFolder}/PreProcessed/QC/b400.png
$FSLDIR/bin/slicer ${sFolder}/Diffusion/mean_b1000.nii.gz -i 0 30 -a ${sFolder}/PreProcessed/QC/b1000.png
$FSLDIR/bin/slicer ${sFolder}/Diffusion/mean_b2600.nii.gz -i 0 10 -a ${sFolder}/PreProcessed/QC/b2600.png


# EDDY QUAD
echo "Run QUAD"
rm -rf ${sFolder}/PreProcessed/eddy/eddy_corrected.qc
field=${sFolder}/PreProcessed/topup/fieldmap
eval `quad_command ${sFolder}/PreProcessed/eddy/eddy_corrected $field`

end=`date +%s`

runtime=$((end-start))
TIME=$(convertsecs $runtime)
echo "Total runtime = $TIME"

echo "Stage 8 complete (${TIME})" >> $sFolder/.status

echo -e "\n END: generatePNGs"
